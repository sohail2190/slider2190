

const teams = [
    {
        id: 1,
        name: "John Doe",
        job: "Software Engineer",
        image: "1.jpg",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec augue vel arcu ultrices consequat."
    },
    {
        id: 2,
        name: "Jane Smith",
        job: "Graphic Designer",
        image: "2.jpg",
        text: "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas."
    },
    {
        id: 3,
        name: "Michael Johnson",
        job: "Data Analyst",
        image: "3.jpg",
        text: "Suspendisse sit amet consectetur elit. Vivamus auctor elit ut nunc varius efficitur."
    },
    {
        id: 4,
        name: "Emily Wilson",
        job: "Marketing Manager",
        image: "4.jpg",
        text: "Integer vel vestibulum elit. Aenean eget nisl velit. Ut feugiat nisi vitae dolor fermentum scelerisque."
    },
    {
        id: 5,
        name: "David Brown",
        job: "Financial Analyst",
        image: "5.jpg",
        text: "Quisque et eros tincidunt, viverra quam sed, feugiat enim. Ut rhoncus, magna nec fermentum tincidunt."
    }
];

export default teams;